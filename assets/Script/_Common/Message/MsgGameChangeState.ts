// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GameState } from "../GameDefs";
import Message, { MessageId } from "./Message";

export default class MsgGameChangeState extends Message {
    gameState: GameState;
    constructor(state:GameState) {
        super();
        this._id = MessageId.GAME_UPDATE_STATE;
        this.gameState = state;
    }
}
