// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

export enum MessageId {
    NONE = 0,
    // Client to server
    GAME_CREATED,
    GAME_UPDATE_STATE,
    GAME_ENDED,
    PLAYER_JOIN_GAME,
    PLAYER_READY,
    PLAYER_SEND_POS_ACT,

    // Server to client
    SERVER_SPAWN_EGGS,
    SERVER_INIT_PLAYER_POS,
    SERVER_START_COUNTDOWN,
    SERVER_START_GAME,
    SERVER_UPDATE_PLAYERS_POS_ACT,
    SERVER_ON_EGG_COLLECTED,
    SERVER_TIME_UP
}

export default class Message {
    protected _id: MessageId = MessageId.NONE;
    get id(): MessageId {return this._id}

    toString():string {
        if (this._id == MessageId.NONE) {
            cc.warn('No message ID is set');
        }
        return JSON.stringify(this);
    }

    fromString(str:string) {
        Object.assign(this, JSON.parse(str));
    }
}
