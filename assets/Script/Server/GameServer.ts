// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { SocketCallbacks } from "../_Common/Socket/FakeSocket";
import Message, { MessageId } from "../_Common/Message/Message";
import MsgGameChangeState from "../_Common/Message/MsgGameChangeState";
import { GetServerSocket } from "../_Common/Socket/ServerSocket";
import MsgPlayerJoin from "../_Common/Message/MsgPlayerJoin";
import GamePlayServer from "./GamePlayServer";
import MsgGameCreated from "../_Common/Message/MsgGameCreated";
import MsgPlayerSendPosAct from "../_Common/Message/MsgPlayerSendPosAct";
import MsgPlayerReady from "../_Common/Message/MsgPlayerReady";

const {ccclass, property} = cc._decorator;


@ccclass
export default class GameServer extends cc.Component implements SocketCallbacks{

    private _gamePlays: GamePlayServer[] = [];
    private get gamePlays():GamePlayServer[] {return this._gamePlays}


    onLoad () {
        // Register socket callbacks
        GetServerSocket().registerCallbacks(this);
    }

    start () {
        cc.log('Start game server');
    }

    update (dt) {
        this.gamePlays.forEach(gamePlay => {
            gamePlay.update(dt);
        });
    }

    onMessage (message: string): void {
        // cc.log('[SERVER] onMessage: ' + message);
        let msg = new Message();
        msg.fromString(message);
        switch (msg.id) {
            case MessageId.GAME_CREATED:
                // create new instance of GamePlayServer
                let createdMsg = msg as MsgGameCreated;
                this.gamePlays.push(new GamePlayServer(createdMsg.width, createdMsg.heigth));
                break;
            case MessageId.GAME_ENDED:
                this.gamePlays.pop(); // Should check client id to remove the right gamePlay instance
                break;
            case MessageId.GAME_UPDATE_STATE:
                this.gamePlays.forEach(gamePlay => {
                    gamePlay.onMsgGameState(msg as MsgGameChangeState); // Should check client id to call the right gamePlay instance instead of calling all them
                });
                break;
            case MessageId.PLAYER_JOIN_GAME:
                this.gamePlays.forEach(gamePlay => {
                    gamePlay.onMsgPlayerJoin(msg as MsgPlayerJoin); // Should check client id to call the right gamePlay instance instead of calling all them
                });
                break;
            case MessageId.PLAYER_READY:
                this.gamePlays.forEach(gamePlay => {
                    gamePlay.onMsgPlayerReady(msg as MsgPlayerReady); // Should check client id to call the right gamePlay instance instead of calling all them
                });
            case MessageId.PLAYER_SEND_POS_ACT:
                this.gamePlays.forEach(gamePlay => {
                    gamePlay.onPlayerUpdatePosAct(msg as MsgPlayerSendPosAct); // Should check client id to call the right gamePlay instance instead of calling all them
                });
                break;
            default:
                // Ignore message ids for client, they won't go here
                break;
        }
    }
}