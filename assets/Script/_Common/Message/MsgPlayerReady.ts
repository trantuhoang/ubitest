// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Message, { MessageId } from "./Message";


export default class MsgPlayerReady extends Message {
    playerId: number;

    constructor(id: number) {
        super();
        this._id = MessageId.PLAYER_READY;
        this.playerId = id;
    }
}