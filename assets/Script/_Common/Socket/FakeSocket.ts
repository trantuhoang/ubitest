// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import FakeNetwork, { RequestType } from "../Network/FakeNetwork";

export interface SocketCallbacks {
    onMessage(message: string): void;
    // Other events (onOpen, onClose, onError...): temporary skipped.
}

export default class FakeSocket {

    private _requestType: RequestType = RequestType.CLIENT_REQUEST;
    private _callbacks: SocketCallbacks = null;

    private _network:FakeNetwork;
    get network():FakeNetwork {return this._network}
    set network(n:FakeNetwork) {this._network = n}

    constructor(isServer: boolean = false) {
        if (isServer) {
            this._requestType = RequestType.SERVER_RESPONSE;
            //GetNetwork().registerServerSocket(this);
        } else {
            //GetNetwork().registerClientSocket(this);
        }
    }

    registerCallbacks (callbacks: SocketCallbacks): void {
        this._callbacks = callbacks;
    }

    onMessage(message: string): void {
        if (this._callbacks == null) {
            cc.error('[SOCKET] No callbacks is set')
        }
        this._callbacks.onMessage(message);
    }
    
    sendMessage(message: string): boolean {
        this.network.requestSendMessage(this._requestType, message);
        return true;
    }

}
