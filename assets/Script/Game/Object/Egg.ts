// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import PlayerBase from "../Player/PlayerBase";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Egg extends cc.Component {

    private _eggId: number;
    get eggId():number {return this._eggId}
    set eggId(id: number) {this._eggId = id}

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.setScale(0, 0);
        this.node.zIndex = cc.game.canvas.height - this.node.position.y;
    }

    start () {
        cc.tween(this.node).to(0.5, {scale: 1}).start();
    }

    // update (dt) {}

    onCollectedBy(player: PlayerBase) {
        let tmove = cc.tween().to(0.3, {position: player.node.position});
        let tscale = cc.tween().to(0.3, {scale: 0});
        cc.tween(this.node).parallel(tmove, tscale).call(()=>{this.node.destroy()}).start();
    }
}
