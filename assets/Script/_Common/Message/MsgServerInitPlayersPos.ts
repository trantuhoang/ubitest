// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Message, { MessageId } from "./Message";

export class PlayerPos {
    playerId: number;
    pos: cc.Vec2;
    constructor(id:number, pos:cc.Vec2) {
        this.playerId = id;
        this.pos = pos;
    }
}

export default class MsgServerInitPlayersPos extends Message {
    playerId: number;
    pos: cc.Vec3;

    constructor(id: number, pos:cc.Vec3) {
        super();
        this._id = MessageId.SERVER_INIT_PLAYER_POS;
        this.playerId = id;
        this.pos = pos;
    }
}
