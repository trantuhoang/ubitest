// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GetGamePlay } from "../GamePlay";
import PlayerBase from "../Player/PlayerBase";

const {ccclass, property} = cc._decorator;

@ccclass
export default class StartMenu extends cc.Component {

    @property(cc.Button)
    btnStart: cc.Button = null;
    
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        GetGamePlay().menuStart = this;
        this.btnStart.node.on('click', this.onStartBtnClick, this);
    }

    start () {}

    // update (dt) {}

    private exit() {
        cc.tween(this.node).to(0.2,{scale: 0.5}).call(()=>{this.node.destroy()}).start();
    }

    onStartBtnClick(btn) {
        this.exit();
        GetGamePlay().startGame();
    }
}