// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import PlayerServer from "../../Server/PlayerServer";
import { PlayerAction } from "../GameDefs";
import Message, { MessageId } from "./Message";
import { PlayerPos } from "./MsgServerInitPlayersPos";

export class PlayerActInfo {
    playerId: number;
    playerAction: PlayerAction;
    constructor(id:number, action:PlayerAction) {
        this.playerId = id;
        this.playerAction = action;
    }
}

export default class MsgServerUpdatePlayersPosAct extends Message {
    players: PlayerServer[];

    constructor() {
        super();
        this._id = MessageId.SERVER_UPDATE_PLAYERS_POS_ACT;
    }
}
