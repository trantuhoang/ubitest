// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Message, { MessageId } from "./Message";
import { EggInfo } from "./MsgServerSpawnEggs";


export default class MsgServerOnEggCollected extends Message {
    collectedEggs: EggInfo[];

    constructor() {
        super();
        this._id = MessageId.SERVER_ON_EGG_COLLECTED;
    }
}