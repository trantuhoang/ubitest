// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Message, { MessageId } from "./Message";


export default class MsgGameEnded extends Message {
    constructor() {
        super();
        this._id = MessageId.GAME_ENDED;
    }
}