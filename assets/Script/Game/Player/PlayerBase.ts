// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { ObjectConst, PlayerAction, PlayerState } from "../../_Common/GameDefs";
import MsgPlayerJoin from "../../_Common/Message/MsgPlayerJoin";
import MsgPlayerReady from "../../_Common/Message/MsgPlayerReady";
import MsgPlayerSendPosAct from "../../_Common/Message/MsgPlayerSendPosAct";
import { GetClientSocket } from "../../_Common/Socket/ClientSocket";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PlayerBase extends cc.Component {

    private _playerId: number = 0;
    get playerId():number {return this._playerId}
    
    private _playerName: string = '';
    get playerName():string {return this.playerName}
    set playerName(name:string) {this._playerName = name}

    private _playerState: PlayerState = PlayerState.NONE;
    get playerState():PlayerState {return this._playerState}

    private _lastAction: PlayerAction = PlayerAction.IDLE;
    get lastAction():PlayerAction {return this._lastAction}
    set lastAction(act:PlayerAction) {this._lastAction = act}

    private _action: PlayerAction = PlayerAction.IDLE;
    get action():PlayerAction {return this._action}
    set action(act:PlayerAction) {
        if (this._action != act) {
            this._lastAction = this._action;
            this._action = act;
        }
    }

    private _eggOwn: number = 0;
    get eggsOwn() {return this._eggOwn}


    private static playerGoAnims: string[] = ['', 'player_goUp', 'player_goDown', 'player_goLeft', 'player_goRight'];
    private static playerIdleAnims: string[] = ['', 'player_idleUp', 'player_idleDown', 'player_idleLeft', 'player_idleRight'];

    private static idCount: number = 0;
    private static generateId(): number {
        return PlayerBase.idCount++;
    }

    constructor() {
        super();
        this._playerId = PlayerBase.generateId();
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        // Join server
        let msg = new MsgPlayerJoin(this._playerId);
        GetClientSocket().sendMessage(msg.toString());
    }

    start () {

    }

    update (dt) {
        switch (this.playerState) {
            case PlayerState.NONE:
                break;
            case PlayerState.JOINED_GAME:
                break;
            case PlayerState.READY_TO_PLAY:
                break;
            case PlayerState.PLAYING:
                {
                    if (this.updateMovement(dt)) {
                        // Send position and action to server
                        let msg = new MsgPlayerSendPosAct(this.playerId, this.node.position, this.action);
                        GetClientSocket().sendMessage(msg.toString());
                        // update z-index to sort render with eggs and other players
                        this.node.zIndex = cc.game.canvas.height - this.node.position.y;
                    }
            
                    this.updateAnimation();
                }
                break;
            default:
                break;
        }
    }

    private updateAnimation(): void {
        if (this.action == PlayerAction.IDLE && this.lastAction == PlayerAction.IDLE) {
            return;
        }
        let anim = this.getComponent(cc.Animation);
        let animName = this.action == PlayerAction.IDLE ? PlayerBase.playerIdleAnims[this.lastAction] : PlayerBase.playerGoAnims[this.action];
        let animState = anim.getAnimationState(animName);
        
        if (animState && !animState.isPlaying) {
            animState.wrapMode = cc.WrapMode.Loop;
            anim.play(animName);
        }
    }

    protected updateMovement(dt): boolean {
        if (this.action == PlayerAction.IDLE) {
            return false;
        }
        let ds = ObjectConst.playerSpeed * dt;
        let maxX = cc.game.canvas.width / 2 - ObjectConst.playerRadius;
        let maxY = cc.game.canvas.height / 2 - ObjectConst.playerRadius;
        let nextX = this.node.position.x;
        let nextY = this.node.position.y;
        switch (this.action) {
            case PlayerAction.GO_LEFT:
                nextX -= ds;
                nextX = Math.max(nextX, -maxX);
                break;
            case PlayerAction.GO_RIGHT:
                nextX += ds;
                nextX = Math.min(nextX, maxX);
                break;
            case PlayerAction.GO_UP:
                nextY += ds;
                nextY = Math.min(nextY, maxY);
                break;
            case PlayerAction.GO_DOWN:
                nextY -= ds;
                nextY = Math.max(nextY, -maxY);
                break;
        }
        if (this.node.position.x != nextX || this.node.position.y != nextY) {
            // cc.tween(this.node).to(dt, {position: targetPos}).start(); ////// Don't know why it doesn't work
            this.node.runAction(cc.moveTo(dt, nextX, nextY));
            return true;
        }
        return false;
    }

    changeState(newState: PlayerState) {
        switch (newState) {
            case PlayerState.NONE:
                this.action = PlayerAction.IDLE;
                this.updateAnimation();
                break;
            case PlayerState.JOINED_GAME:
                break;
            case PlayerState.READY_TO_PLAY:
                let msg = new MsgPlayerReady(this.playerId);
                GetClientSocket().sendMessage(msg.toString());
                break;
            case PlayerState.PLAYING:
                break;
            default:
                break;
        }
        this._playerState = newState;
    }

    protected removeAction(action: PlayerAction): void {
        if (this.action == action) {
            this.lastAction = this.action;
            this.action = PlayerAction.IDLE;
        }
    }

    onCollectEgg() {
        this._eggOwn++;
    }
}
