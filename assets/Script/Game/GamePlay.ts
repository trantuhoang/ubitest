// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Game from "./Game";
import { GamePlayConst, GameState, PlayerState } from "../_Common/GameDefs";
import { GetClientSocket } from "../_Common/Socket/ClientSocket";
import MsgGameCreated from "../_Common/Message/MsgGameCreated";
import MsgServerInitPlayersPos from "../_Common/Message/MsgServerInitPlayersPos";
import MsgServerSpawnEggs from "../_Common/Message/MsgServerSpawnEggs";
import MsgServerOnEggCollected from "../_Common/Message/MsgServerOnEggCollected";
import MsgServerUpdatePlayersPosAct from "../_Common/Message/MsgServerUpdatePlayersPosAct";
import MsgServerStartCountdown from "../_Common/Message/MsgServerStartCountdown";
import MsgServerStartGame from "../_Common/Message/MsgServerStartGame";
import MsgServerTimeUp from "../_Common/Message/MsgServerTimeUp";
import AI from "./Player/AI";
import Egg from "./Object/Egg";
import Player from "./Player/Player";
import PlayerBase from "./Player/PlayerBase";
import MsgGameEnded from "../_Common/Message/MsgGameEnded";
import TimeUpMenu from "./Menu/TimeUpMenu";
import StartMenu from "./Menu/StartMenu";
import Countdown from "./Menu/Countdown";

export default class GamePlay {
    // Singleton implementation
    private static s_instance: GamePlay = null;
    static instance(): GamePlay {
        if (GamePlay.s_instance == null) {
            GamePlay.s_instance = new GamePlay();
        }
        return GamePlay.s_instance;
    }
    // ----------

    private _game: Game;
    get game(): Game {return this._game}
    set game(g: Game) {this._game = g}

    private _menuTimeUp: TimeUpMenu;
    get menuTimeUp(): TimeUpMenu {return this._menuTimeUp}
    set menuTimeUp(menu: TimeUpMenu) {this._menuTimeUp = menu}

    private _menuStart: StartMenu;
    get menuStart(): StartMenu {return this._menuStart}
    set menuStart(menu: StartMenu) {this._menuStart = menu}

    private _menuCountdown: Countdown;
    get menuCountdown(): Countdown {return this._menuCountdown}
    set menuCountdown(menu: Countdown) {this._menuCountdown = menu}

    
    private _gameState: GameState = GameState.NONE;
    get gameState():GameState {return this._gameState}

    private _mainPlayer:Player;
    private _aiPlayers:AI[] = [];

    private _eggs:Egg[] = [];
    get eggs():Egg[] {return this._eggs}

    private createGame() {
        // Send msg create game
        let msgGameCreated = new MsgGameCreated(cc.game.canvas.width, cc.game.canvas.height);
        GetClientSocket().sendMessage(msgGameCreated.toString());

        this.changeGameState(GameState.GAME_CREATED);
    }

    startGame() {
        this.createGame();
    }
    
    restartGame() {
        //remove old objects
        this._mainPlayer.node.destroy();
        this._mainPlayer = null;
        this._aiPlayers.forEach(p => {p.node.destroy()});
        this._aiPlayers = [];
        this._eggs.forEach(e => {e.node.destroy()});
        this._eggs = [];
        this.game.labelScore.string = 'Your eggs: 0';
        //create new game
        this.createGame();
    }

    onLoad () {
        this.changeGameState(GameState.LOADING);
    }

    onStart () {}

    update (dt) {
        switch (this.gameState) {
            case GameState.NONE:
                break;
            case GameState.LOADING:
                break;
            case GameState.GAME_CREATED:
                break;
            case GameState.GAME_READY:
                break;
            case GameState.GAME_PLAYING:
                break;
            case GameState.GAME_END:
                break;
            default:
                break;
        }
    }

    private changeGameState(newState: GameState) {
        switch (newState) {
            case GameState.NONE:
                break;
            case GameState.LOADING: // not support now
                break;
            case GameState.GAME_CREATED:
                {
                    // Create main player
                    let playerNode = cc.instantiate(this.game.prefabMainPlayer);
                    this.game.node.addChild(playerNode);
                    this._mainPlayer = playerNode.getComponent('Player') as Player;

                    // Generate players
                    this.generateAIPlayers();

                    // AI players ready
                    this._aiPlayers.forEach(p => {p.changeState(PlayerState.READY_TO_PLAY)});
            
                    // Main player ready
                    this._mainPlayer.changeState(PlayerState.READY_TO_PLAY);
                }
                break;
            case GameState.GAME_READY:
                //Todo: start counting down
                break;
            case GameState.GAME_PLAYING:
                {
                    this._mainPlayer.changeState(PlayerState.PLAYING)
                    this._aiPlayers.forEach(p => {
                        p.changeState(PlayerState.PLAYING);
                    });
                }
                break;
            case GameState.GAME_END:
                {
                    this._mainPlayer.changeState(PlayerState.NONE);
                    this._aiPlayers.forEach(p => {
                        p.changeState(PlayerState.NONE);
                    });

                    
                    // let topPlayers = this._aiPlayers.sort((a, b) => a.eggsOwn - b.eggsOwn);
                    // let num = Math.min(3, topPlayers.length)
                    // for (let i = 0; i < num; i++) {
                    //     let p = topPlayers[i];
                    //     // this.labelTopPlayers.string += p.playerName + ': ' + p.eggsOwn + '\n';
                    // }
                    
                    let maxEggs = this._aiPlayers.sort((a, b) => a.eggsOwn - b.eggsOwn)[0].eggsOwn;
                    let win = this._mainPlayer.eggsOwn > maxEggs;
                    this.menuTimeUp.display(win, this._mainPlayer, this._aiPlayers);

                    let msg = new MsgGameEnded();
                    GetClientSocket().sendMessage(msg.toString());
                }
                break;
            default:
                break;
        }
        this._gameState = newState;
    }

    private generateAIPlayers() {
        for(let i = 0; i < GamePlayConst.numAIPlayers; i++) {
            let playerNode = cc.instantiate(this.game.prefabAIPlayer);
            this.game.node.addChild(playerNode);
            // set game reference
            let ai = playerNode.getComponent('AI') as AI;
            this._aiPlayers.push(ai);
        }
    }

    private getAIPlayer(id:number):AI {
        return this._aiPlayers.find(p => {return p.playerId == id});
    }

    private getPlayer(id:number):PlayerBase {
        if (id == this._mainPlayer.playerId) {
            return this._mainPlayer as PlayerBase;
        }
        return this._aiPlayers.find(p => {return p.playerId == id});
    }

    onMsgStartCountdown(msg: MsgServerStartCountdown) {
        this.changeGameState(GameState.GAME_READY);
        this.menuCountdown.startCountdown(msg.time);
    }

    onMsgStartGame(msg: MsgServerStartGame) {
        this.changeGameState(GameState.GAME_PLAYING);
    }

    onMsgTimeUp(msg: MsgServerTimeUp) {
        // Sync player eggOwn num
        msg.players.forEach(p => {
            let player = this.getPlayer(p.id);
            if (player.eggsOwn != p.eggsOwn) {
                cc.log('Player ' + player.playerId + ' owns wrong number of eggs');
                //Todo: sync the eggs number
            }
        });
        // change game state
        this.changeGameState(GameState.GAME_END);
    }

    onMsgInitPlayerPos(msg: MsgServerInitPlayersPos) {
        this.getPlayer(msg.playerId).node.position = msg.pos;
    }

    onMsgUpdatePlayersPosAct(msg: MsgServerUpdatePlayersPosAct) {
        msg.players.forEach(serverP => {
            let ai = this.getAIPlayer(serverP.id);
            if (ai) {
                //Todo:sync
            }
        })
    }

    onMsgSpawnEggs(msg: MsgServerSpawnEggs) {
        msg.eggs.forEach(eggInfo => {
            if (undefined == this._eggs.find(e => {return e.eggId == eggInfo.id})) {
                let eggNode = cc.instantiate(this.game.prefabEgg);
                eggNode.setPosition(eggInfo.pos);
                this.game.node.addChild(eggNode);
                // set game reference
                let egg = eggNode.getComponent('Egg') as Egg;
                egg.eggId = eggInfo.id;
                this._eggs.push(egg);
            }
        });
    }

    onMsgCollectEgg(msg: MsgServerOnEggCollected) {
        msg.collectedEggs.forEach(ownedEgg => {
            let eggIdx = this._eggs.findIndex(e => {return e.eggId == ownedEgg.id})
            let player = this.getPlayer(ownedEgg.collectedBy);
            this._eggs[eggIdx].onCollectedBy(player);
            this._eggs.splice(eggIdx, 1);
            player.onCollectEgg();
            //Update score label
            if (player.playerId == this._mainPlayer.playerId) {
                this.game.labelScore.string = 'Your eggs: ' + player.eggsOwn;
            }
        });
    }
}

export function GetGamePlay():GamePlay {
    return GamePlay.instance();
}