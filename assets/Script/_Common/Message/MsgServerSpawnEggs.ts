// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Message, { MessageId } from "./Message";

export class EggInfo {
    id: number;
    pos: cc.Vec3;
    collectedBy:number = undefined; // player id
    constructor(id:number, pos:cc.Vec3) {
        this.id = id;
        this.pos = pos;
    }
}

export default class MsgServerSpawnEggs extends Message {
    eggs: EggInfo[] = [];

    constructor() {
        super();
        this._id = MessageId.SERVER_SPAWN_EGGS;
    }

    addEgg(info: EggInfo) {
        this.eggs.push(info);
    }
}