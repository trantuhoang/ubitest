// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

export enum GameState {
    NONE,
    LOADING,
    GAME_CREATED,
    GAME_READY,
    GAME_PLAYING,
    GAME_END
}

export enum PlayerState {
    NONE,
    JOINED_GAME,
    READY_TO_PLAY,
    PLAYING
}

export enum PlayerAction {
    IDLE = 0,
    GO_UP,
    GO_DOWN,
    GO_LEFT,
    GO_RIGHT
}

export const GameServerConst = {
    minIntervalSyncGame: 0.1, //in sec, min interval time that server will send data to client to sync
    maxIntervalSyncGame: 0.5, //in sec, max interval time that server will send data to client to sync
} as const;

export const GamePlayConst = {
    numAIPlayers: 5, // number of AI players
    numInitEggs: 5, // number of eggs will be generated at starting game
    countdownTime: 3, //in sec, count down time before starting game
    gameTime: 30, //in sec, time of a game.
} as const;

export const ObjectConst = {
    eggRadius: 10, // egg's logical radius
    playerRadius: 15, // player's logical radius

    playerSpeed: 80, // player's speed (units of resolution per sec)
} as const;

