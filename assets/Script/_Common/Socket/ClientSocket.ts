// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import FakeSocket from "./FakeSocket";
import ServerSocket from "./ServerSocket";


export default class ClientSocket extends FakeSocket {

    // Singleton implementation
    private static s_instance: ClientSocket = null;
    static instance(): ClientSocket {
        if (ClientSocket.s_instance == null) {
            ClientSocket.s_instance = new ClientSocket();
        }
        return ClientSocket.s_instance;
    }

    private constructor() {
        super();
    }
    // ----------
}

export function GetClientSocket():ClientSocket {
    return ClientSocket.instance();
}

export function GetServerSocket():ServerSocket {
    return ServerSocket.instance();
}