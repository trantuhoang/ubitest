// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { ObjectConst, PlayerAction } from "../../_Common/GameDefs";
import MsgPlayerSendPosAct from "../../_Common/Message/MsgPlayerSendPosAct";
import { GetClientSocket } from "../../_Common/Socket/ClientSocket";
import PlayerBase from "./PlayerBase";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends PlayerBase {

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        super.onLoad();
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    start () {
        super.start();
    }

    update (dt) {
        super.update(dt);
    }

    private onKeyDown(evt: cc.Event.EventKeyboard) {
        switch(evt.keyCode) {
            case cc.macro.KEY.a:
                this.action = PlayerAction.GO_LEFT;
                break;
            case cc.macro.KEY.d:
                this.action = PlayerAction.GO_RIGHT;
                break;
            case cc.macro.KEY.w:
                this.action = PlayerAction.GO_UP;
                break;
            case cc.macro.KEY.s:
                this.action = PlayerAction.GO_DOWN;
                break;
            default:
                break;
        }
    }

    private onKeyUp(evt: cc.Event.EventKeyboard) {
        switch(evt.keyCode) {
            case cc.macro.KEY.a:
                this.removeAction(PlayerAction.GO_LEFT);
                break;
            case cc.macro.KEY.d:
                this.removeAction(PlayerAction.GO_RIGHT);
                break;
            case cc.macro.KEY.w:
                this.removeAction(PlayerAction.GO_UP);
                break;
            case cc.macro.KEY.s:
                this.removeAction(PlayerAction.GO_DOWN);
                break;
            default:
                break;
        }
    }
}
