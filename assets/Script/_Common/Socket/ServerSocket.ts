// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import FakeSocket from "./FakeSocket";


export default class ServerSocket extends FakeSocket {

    // Singleton implementation
    private static s_instance: ServerSocket = null;
    static instance(): ServerSocket {
        if (ServerSocket.s_instance == null) {
            ServerSocket.s_instance = new ServerSocket();
        }
        return ServerSocket.s_instance;
    }

    private constructor() {
        super(true);
    }
    // ----------
}

export function GetServerSocket():ServerSocket {
    return ServerSocket.instance();
}
