// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

// import { GetServerSocket } from "../Socket/ServerSocket";
import { GetClientSocket, GetServerSocket } from "../Socket/ClientSocket";
import FakeSocket from "../Socket/FakeSocket";


const {ccclass, property} = cc._decorator;

export enum RequestType {
    CLIENT_REQUEST,
    SERVER_RESPONSE
}

interface Request {
    type: RequestType,
    data: string
}

@ccclass
export default class FakeNetwork extends cc.Component {
    // Simulate latency
    private simulateLatency: boolean = false;
    private minLatency: number = 0;
    private maxLatency: number = 0;

    private clientRequests: Request[] = [];
    private serverRequests: Request[] = [];
    private timerClientReq: number = 0;
    private timerServerReq: number = 0;
    private clientSocket: FakeSocket = null;
    private serverSocket: FakeSocket = null;

    registerClientSocket(socket: FakeSocket): void {
        this.clientSocket = socket;
    }
    registerServerSocket(socket: FakeSocket): void {
        this.serverSocket = socket;
    }

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.clientSocket = GetClientSocket();
        this.serverSocket = GetServerSocket();
        this.clientSocket.network = this;
        this.serverSocket.network = this;
    }

    start () {
        cc.log('Start fake network')
    }

    update (dt) {
        this.updateClientRequests(dt);
        this.updateServerRequests(dt);
    }

    private updateClientRequests(dt) {
        // Simulate network latency
        if (this.simulateLatency) {
            this.timerClientReq += dt
            let latency = this.minLatency + Math.random() * (this.maxLatency - this.minLatency);
            if (latency > this.timerClientReq) {
                return;
            }
            this.timerClientReq = 0;
        }
        
        // Handle requests
        if (this.clientRequests.length <= 0) {
            return;
        }

        while (this.clientRequests.length > 0) {
            this.processRequest (this.clientRequests.shift());
        }
    }

    private updateServerRequests(dt) {
        // Simulate network latency
        if (this.simulateLatency) {
            this.timerServerReq += dt
            let latency = this.minLatency + Math.random() * (this.maxLatency - this.minLatency);
            if (latency > this.timerServerReq) {
                return;
            }
            this.timerServerReq = 0;
        }
        
        // Handle requests
        if (this.serverRequests.length <= 0) {
            return;
        }
        while (this.serverRequests.length > 0) {
            this.processRequest (this.serverRequests.shift());
        }
    }

    requestSendMessage (type: RequestType, message: string): void {
        let req: Request = {type: type, data: message};
        if (type == RequestType.CLIENT_REQUEST) {
            this.clientRequests.push(req);
        } else {
            this.serverRequests.push(req);
        }
    }

    private processRequest (req: Request): void {
        switch (req.type) {
            case RequestType.CLIENT_REQUEST:
                if (this.serverSocket == null) {
                    cc.error('[NETWORK] No server socket is set');
                    return;
                }
                this.serverSocket.onMessage(req.data);
                break;
            case RequestType.SERVER_RESPONSE:
                if (this.clientSocket == null) {
                    cc.error('[NETWORK] No client socket is set');
                    return;
                }
                this.clientSocket.onMessage(req.data);
                break;
        }
    }
}