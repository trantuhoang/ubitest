// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GetGamePlay } from "../GamePlay";
import PlayerBase from "../Player/PlayerBase";

const {ccclass, property} = cc._decorator;

@ccclass
export default class TimeUpMenu extends cc.Component {

    @property(cc.Label)
    labelYourEggs: cc.Label = null;

    @property(cc.Label)
    labelTopPlayers: cc.Label = null;

    @property(cc.Button)
    btnRestart: cc.Button = null;
    
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.active = false;
        this.node.scale = 0.5;
        GetGamePlay().menuTimeUp = this;
        this.btnRestart.node.on('click', this.onRestartBtnClick, this);
    }

    start () {}

    // update (dt) {}

    display(win: boolean, mainPlayer: PlayerBase, others: PlayerBase[]) {
        if (win) {
            this.labelYourEggs.string = 'You WIN with ' + mainPlayer.eggsOwn + ' eggs';
        } else {
            this.labelYourEggs.string = 'Your eggs: ' + mainPlayer.eggsOwn;
        }
        
        this.node.active = true;
        cc.tween(this.node).to(0.2, {scale: 1}).start();
    }

    hide() {
        cc.tween(this.node).to(0.2,{scale: 0.5}).call(()=>{this.node.active = false;}).start();
    }

    onRestartBtnClick(btn) {
        this.hide();
        GetGamePlay().restartGame();
    }
}
