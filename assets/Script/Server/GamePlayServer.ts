// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GamePlayConst, ObjectConst, GameServerConst, GameState, PlayerState } from "../_Common/GameDefs";
import MsgPlayerSendPosAct from "../_Common/Message/MsgPlayerSendPosAct";
import MsgPlayerJoin from "../_Common/Message/MsgPlayerJoin";
import MsgPlayerReady from "../_Common/Message/MsgPlayerReady";
import MsgGameChangeState from "../_Common/Message/MsgGameChangeState";
import PlayerServer from "./PlayerServer";
import MsgServerUpdatePlayersPosAct from "../_Common/Message/MsgServerUpdatePlayersPosAct";
import { GetServerSocket } from "../_Common/Socket/ServerSocket";
import MsgServerSpawnEggs, { EggInfo } from "../_Common/Message/MsgServerSpawnEggs";
import MsgServerOnEggCollected from "../_Common/Message/MsgServerOnEggCollected";
import MsgServerInitPlayersPos from "../_Common/Message/MsgServerInitPlayersPos";
import MsgServerStartCountdown from "../_Common/Message/MsgServerStartCountdown";
import MsgServerStartGame from "../_Common/Message/MsgServerStartGame";
import MsgServerTimeUp from "../_Common/Message/MsgServerTimeUp";


export default class GamePlayServer {

    private _gameW:number;
    get gameW():number {return this._gameW}
    set gameW(w:number) {this._gameW = w}
    
    private _gameH: number;
    get gameH():number {return this._gameH}
    set gameH(h: number) {this._gameH = h}

    private _gameState: GameState = GameState.NONE;
    get gameState():GameState {return this._gameState}
    // set gameState(state: GameState) {this._gameState = state}

    private _players: PlayerServer[] = [];
    private _eggs:EggInfo[] = [];
    private _lastEggId: number = 0;

    private _countdownTimer: number;
    private _gameTimer: number;
    private _syncTimer: number;

    constructor(w:number, h:number) {
        this._gameW = w;
        this._gameH = h;
        this.changeGameState(GameState.GAME_CREATED);
    }

    update(dt) {
        // Update players
        this._players.forEach(p => {p.udpate(dt)});

        switch(this.gameState) {
            case GameState.NONE:
                break;
            case GameState.GAME_CREATED:
                {
                    // Check if all players ready to play
                    let unready = this._players.find(p => {return p.state != PlayerState.READY_TO_PLAY});
                    if (undefined == unready) {
                        // Change game state
                        this.changeGameState(GameState.GAME_READY);
                    }
                }
                break;
            case GameState.GAME_READY:
                {
                    this._countdownTimer -= dt;
                    if (this._countdownTimer <= 0) {
                        this.changeGameState(GameState.GAME_PLAYING);
                    }
                }
                break;
            case GameState.GAME_PLAYING:
                {
                    // check game time
                    {
                        this._gameTimer += dt;
                        if (this._gameTimer > GamePlayConst.gameTime) {
                            this.changeGameState(GameState.GAME_END);
                            break;
                        }
                    }

                    // Send players's pos and action to client
                    {
                        this._syncTimer += dt;
                        let randomInterval = GameServerConst.minIntervalSyncGame + (Math.random() * (GameServerConst.maxIntervalSyncGame - GameServerConst.minIntervalSyncGame));
                        // No need to send now because the ai is run on the same game host with the main player
                        if (false && this._syncTimer > randomInterval) {
                            let msg = new MsgServerUpdatePlayersPosAct();
                            msg.players = this._players;
                            GetServerSocket().sendMessage(msg.toString());
                
                            // Reset timer
                            this._syncTimer = 0;
                        }
                    }
            
                    // Check collecting egg
                    this.checkCollectEggs();

                    // Generate eggs when they're too few left
                    {
                        let eggsLeft = this._eggs.filter(e => {return e.collectedBy == undefined}).length;
                        let playerNum = this._players.length;
                        if (eggsLeft / playerNum < 0.5) {
                            this.spawnEggs(playerNum - eggsLeft);
                        }
                    }
                }
                break;
            case GameState.GAME_END:
                break;
            default:
                break;
        }
        
    }

    private changeGameState(newState: GameState) {
        switch (newState) {
            case GameState.NONE:
                break;
            case GameState.LOADING:
                break;
            case GameState.GAME_CREATED:
                break;
            case GameState.GAME_READY:
                {
                    this.spawnEggs(GamePlayConst.numInitEggs);

                    // Notify counting down to start game
                    {
                        this._countdownTimer = GamePlayConst.countdownTime
                        let msg = new MsgServerStartCountdown(this._countdownTimer);
                        GetServerSocket().sendMessage(msg.toString());
                    }
                }
                break;
            case GameState.GAME_PLAYING:
                {
                    // Notify game start to client
                    let msg = new MsgServerStartGame();
                    GetServerSocket().sendMessage(msg.toString());

                    //
                    this._gameTimer = 0;
                    this._syncTimer = 0;
                }
                break;
            case GameState.GAME_END:
                {
                    // Notify time up to client
                    let msg = new MsgServerTimeUp();
                    msg.players = this._players;
                    GetServerSocket().sendMessage(msg.toString());
                }
                break;
            default:
                break;
        }
        this._gameState = newState;
    }

    private spawnEggs(num:number) {
        for (let i = 0; i < num; i++) {
            let egg = new EggInfo(this._lastEggId++, this.randomEggPos());
            this._eggs.push(egg);
        }
        // Send eggs to client
        {
            let msg = new MsgServerSpawnEggs();
            msg.eggs = this._eggs.filter(e => e.collectedBy == undefined);
            GetServerSocket().sendMessage(msg.toString());
        }
    }

    private randomEggPos():cc.Vec3 {
        let pos = cc.v3();
        let needNewPos = true;
        while (needNewPos) {
            pos.x = Math.random() * this._gameW - this._gameW / 2;
            pos.y = Math.random() * this._gameH - this._gameH / 2;
            // Make sure it doesn't overlap other eggs
            let overlap = this._eggs.filter(e => {return e.collectedBy == undefined}).find(egg => {
                let dist = egg.pos.sub(pos).mag();
                return dist <= 2 * ObjectConst.eggRadius;
            });
            needNewPos = overlap != undefined;
            if (!needNewPos) {
                // Make sure it doesn't overlap players
                let overlap = this._players.find(player => {
                    let dist = player.pos.sub(pos).mag();
                    return dist <= ObjectConst.eggRadius + ObjectConst.playerRadius;
                });
                needNewPos = overlap != undefined;
            }
        }
        return pos;
    }

    private randomPlayerPos():cc.Vec3 {
        let pos = cc.v3();
        let needNewPos = true;
        while (needNewPos) {
            pos.x = Math.random() * this._gameW - this._gameW / 2;
            pos.y = Math.random() * this._gameH - this._gameH / 2;
            // Make sure it doesn't overlap other eggs
            let overlap = this._eggs.find(egg => {
                let dist = egg.pos.sub(pos).mag();
                return dist <= 2 * ObjectConst.eggRadius;
            });
            needNewPos = overlap != undefined;
            if (!needNewPos) {
                // Make sure it doesn't overlap players
                let overlap = this._players.find(player => {
                    let dist = player.pos.sub(pos).mag();
                    return dist <= ObjectConst.eggRadius + ObjectConst.playerRadius;
                });
                needNewPos = overlap != undefined;
            }
        }
        return pos;
    }

    private checkCollectEggs() {
        let newCollectedEggs: EggInfo[] = [];
        this._eggs.filter(egg => {return egg.collectedBy == undefined}).forEach(egg => {
            let minDist = ObjectConst.playerRadius + ObjectConst.eggRadius;
            this._players.forEach(player => {
                let dist = egg.pos.sub(player.pos).mag();
                if (dist < ObjectConst.playerRadius + ObjectConst.eggRadius && dist < minDist) {
                    egg.collectedBy = player.id;
                }
            });
            if (egg.collectedBy != undefined) {
                newCollectedEggs.push(egg);
            }
        });
        
        // Send eggs collected to client
        if (newCollectedEggs.length > 0) {
            let msg = new MsgServerOnEggCollected();
            msg.collectedEggs = newCollectedEggs;
            GetServerSocket().sendMessage(msg.toString());
        }
    }

    onMsgPlayerJoin(msg: MsgPlayerJoin) {
        let newPlayer = new PlayerServer(msg.playerId);
        newPlayer.pos = this.randomPlayerPos();
        this._players.push(newPlayer);
        // Response the player pos
        let resMsg = new MsgServerInitPlayersPos(newPlayer.id, newPlayer.pos);
        GetServerSocket().sendMessage(resMsg.toString());
    }

    onMsgPlayerReady(msg: MsgPlayerReady) {
        this._players.find(player => {return player.id == msg.playerId}).state = PlayerState.READY_TO_PLAY;
    }

    onMsgGameState(msg: MsgGameChangeState) {
        // This message no need at current
        // this._gameState = msg.gameState;
    }

    onPlayerUpdatePosAct(msg: MsgPlayerSendPosAct) {
        let player = this._players.find(player => {return player.id == msg.playerId})
        if (player != undefined) {
            player.action = msg.action;
            player.pos.x = msg.pos.x;
            player.pos.y = msg.pos.y;
        }
    }

}