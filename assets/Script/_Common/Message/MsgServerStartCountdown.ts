// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { PlayerAction } from "../GameDefs";
import Message, { MessageId } from "./Message";


export default class MsgServerStartCountdown extends Message {
    time: number; //in seconds
    
    constructor(sec: number) {
        super();
        this._id = MessageId.SERVER_START_COUNTDOWN;
        this.time = sec;
    }
}