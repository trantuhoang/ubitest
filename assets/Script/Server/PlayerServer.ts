// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { PlayerAction, PlayerState } from "../_Common/GameDefs";


export default class PlayerServer {
    id: number;
    pos: cc.Vec3 = cc.v3();
    state: PlayerState;
    action: PlayerAction = PlayerAction.IDLE;
    eggsOwn: number = 0;

    constructor(id: number) {
        this.id = id;
    }

    udpate(dt) {

    }

    onCollectEgg() {
        this.eggsOwn++;
    }
}
