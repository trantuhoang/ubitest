// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { GetGamePlay } from "../GamePlay";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Countdown extends cc.Component {

    @property(cc.Label)
    labelCountdown: cc.Label = null;

    private _countdownTimer: number = 0;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.active = false;
        GetGamePlay().menuCountdown = this;
    }

    start () {

    }

    update (dt) {
        if (this._countdownTimer > 0) {
            this._countdownTimer -= dt;
            let ceil = Math.ceil(this._countdownTimer);
            ceil = Math.max(1, ceil);
            this.labelCountdown.string = ceil.toString();
            return;
        }
        if (this.node.active) {
            this.node.active = false;
        }
    }

    startCountdown(time: number) {
        this.node.active = true;
        this._countdownTimer = time;
    }
}
