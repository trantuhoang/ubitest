// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { PlayerAction } from "../GameDefs";
import Message, { MessageId } from "./Message";


export default class MsgPlayerSendPosAct extends Message {
    playerId: number;
    pos: cc.Vec3;
    action: PlayerAction;

    constructor(id: number, pos:cc.Vec3, act: PlayerAction) {
        super();
        this._id = MessageId.PLAYER_SEND_POS_ACT;
        this.playerId = id;
        this.pos = pos;
        this.action = act;
    }
}