// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { PlayerAction } from "../../_Common/GameDefs";
import { GetGamePlay } from "../GamePlay";
import PlayerBase from "./PlayerBase";

enum AILevel {
    Easy,
    Medium,
    Smart,
    // higher so on
}

enum AIPrefer {
    HorizontalMove,
    VerticalMove,
}

const {ccclass, property} = cc._decorator;

@ccclass
export default class AI extends PlayerBase {
    private static s_playerNames: string[] = ['Oliver', 'Jake', 'Noah', 'James', 'Jack', 'Connor', 'Liam', 'John', 'Harry', 'Callum',
                                    'Mason', 'Robert', 'Jacob', 'Kyle', 'William', 'Joe', 'Ethan', 'David', 'George', 'Reece', 
                                    'Michael', 'Richard', 'Oscar', 'Rhys', 'Alexander', 'Joseph', 'Charlie', 'Charles', 'Damian', 'Daniel', 'Thomas',
                                    'Amelia', 'Emma', 'Mary', 'Samantha', 'Olivia', 'Patricia', 'Isla', 'Bethany', 'Sophia', 'Jennifer', 'Elizabeth', 
                                    'Poppy', 'Joanne', 'Linda', 'Ava', 'Megan', 'Mia', 'Barbara', 'Isabella', 'Victoria', 'Emily', 'Susan', 'Lauren', 
                                    'Abigail', 'Margaret', 'Lily', 'Michelle', 'Madison', 'Jessica', 'Sophie', 'Tracy', 'Charlotte', 'Sarah'];
    private _prefer: AIPrefer;
    private _level: AILevel;
    private _simpleTimer = 0;


    constructor() {
        super();
        let idx = Math.round(Math.random() * (AI.s_playerNames.length - 1));
        this.playerName = AI.s_playerNames[idx];
    }

    onLoad () {
        super.onLoad();
        this._level = Math.random() < 0.5 ? AILevel.Easy : AILevel.Medium; //no smart now
        this._prefer = Math.random() < 0.5 ? AIPrefer.VerticalMove : AIPrefer.HorizontalMove;
    }

    update (dt) {
        super.update(dt);
        // AI
        this.updateAI(dt);
    }

    private updateAI(dt) {
        switch (this._level) {
            case AILevel.Easy:
                this._simpleTimer += dt;
                let time = 1 + Math.random() * 3;
                if (this._simpleTimer > time) {
                    this.decideRandomAction();
                    this._simpleTimer = 0;
                }
                break;
            case AILevel.Medium:
                this.decideActionByNearestEgg();
                break;
            default:
                break;
        }
        // Decide the next direction


    }

    private decideRandomAction() {
        let action = Math.round(Math.random() * 5 % 5);
        this.action = action as PlayerAction;
        if (this.action == this.lastAction) {
            let act = this.action as number;
            act = (act + 1) % 5;
            this.action = act as PlayerAction;
        }
    }

    private decideActionByNearestEgg() {
        let nearestPos = this.findNearestEggPos();
        let distX = Math.abs(this.node.position.x - nearestPos.x);
        let distY = Math.abs(this.node.position.y - nearestPos.y);
        let horizontalMove = this._prefer == AIPrefer.HorizontalMove ? (distX / distY > 0.5 ? true : false) : (distX / distY > 1.5);
        if (horizontalMove) {
            // Move horizontal
            this.action = nearestPos.x < this.node.position.x ? PlayerAction.GO_LEFT : PlayerAction.GO_RIGHT;
        } else {
            this.action = nearestPos.y < this.node.position.y ? PlayerAction.GO_DOWN : PlayerAction.GO_UP;
        }
    }

    private findNearestEggPos():cc.Vec3 {
        let minDist = cc.game.canvas.width + cc.game.canvas.height;
        let pos:cc.Vec3 = null;
        GetGamePlay().eggs.forEach(e => {
            let dist = e.node.position.sub(this.node.position).mag();
            if (dist < minDist) {
                minDist = dist;
                pos = e.node.position;
            }
        })
        return pos;
    }
}
