import { SocketCallbacks } from "../_Common/Socket/FakeSocket";
import { GetClientSocket } from "../_Common/Socket/ClientSocket";
import Message, { MessageId } from "../_Common/Message/Message";
import MsgServerInitPlayersPos from "../_Common/Message/MsgServerInitPlayersPos";
import MsgServerSpawnEggs from "../_Common/Message/MsgServerSpawnEggs";
import MsgServerOnEggCollected from "../_Common/Message/MsgServerOnEggCollected";
import MsgServerUpdatePlayersPosAct from "../_Common/Message/MsgServerUpdatePlayersPosAct";
import MsgServerStartCountdown from "../_Common/Message/MsgServerStartCountdown";
import MsgServerStartGame from "../_Common/Message/MsgServerStartGame";

import MsgServerTimeUp from "../_Common/Message/MsgServerTimeUp";
import { GetGamePlay } from "./GamePlay";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Game extends cc.Component implements SocketCallbacks{

    @property(cc.Label)
    labelScore: cc.Label = null;

    @property(cc.Prefab)
    prefabMainPlayer: cc.Prefab = null;

    @property(cc.Prefab)
    prefabAIPlayer: cc.Prefab = null;

    @property(cc.Prefab)
    prefabEgg: cc.Prefab = null;
    
    onLoad () {
        // Register socket callbacks
        GetClientSocket().registerCallbacks(this);
        // Init Game play
        GetGamePlay().game = this;
        GetGamePlay().onLoad();
    }

    start () {
        GetGamePlay().onStart();
    }

    update (dt) {
        GetGamePlay().update(dt);
    }

    onMessage (message: string): void {
        // cc.log('[GAME] onMessage: ' + message);
        let msg = new Message();
        msg.fromString(message);
        switch (msg.id) {
            case MessageId.SERVER_START_COUNTDOWN:
                GetGamePlay().onMsgStartCountdown(msg as MsgServerStartCountdown);
                break;
            case MessageId.SERVER_START_GAME:
                GetGamePlay().onMsgStartGame(msg as MsgServerStartGame);
                break;
            case MessageId.SERVER_TIME_UP:
                GetGamePlay().onMsgTimeUp(msg as MsgServerTimeUp);
                break;
            case MessageId.SERVER_SPAWN_EGGS:
                GetGamePlay().onMsgSpawnEggs(msg as MsgServerSpawnEggs);
                break;
            case MessageId.SERVER_ON_EGG_COLLECTED:
                GetGamePlay().onMsgCollectEgg(msg as MsgServerOnEggCollected);
                break;
            case MessageId.SERVER_INIT_PLAYER_POS:
                GetGamePlay().onMsgInitPlayerPos(msg as MsgServerInitPlayersPos);
                break;
            case MessageId.SERVER_UPDATE_PLAYERS_POS_ACT:
                GetGamePlay().onMsgUpdatePlayersPosAct(msg as MsgServerUpdatePlayersPosAct);
                break;
            default:
                // Ignore message ids for server, they won't go here
                break;
        }
    }
}