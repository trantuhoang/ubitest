// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import PlayerServer from "../../Server/PlayerServer";
import { PlayerAction } from "../GameDefs";
import Message, { MessageId } from "./Message";


export default class MsgServerTimeUp extends Message {
    players: PlayerServer[];
    constructor() {
        super();
        this._id = MessageId.SERVER_TIME_UP;
    }
}